package com.guntreeapp.controller;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.guntreeapp.model.Gender;
import com.guntreeapp.service.ContactManager;


/**
 * @author 
 *
 */
@Controller
public class GreetingController {

    @Autowired
    private ContactManager contactManager;
    
    /**
     * @param model
     * @return
     * @throws ParseException
     */
    @RequestMapping(value = "/greeting", method = RequestMethod.GET)
	public String getAllGreeting(Model model) throws ParseException
	{
    	model.addAttribute("greeting");
		return "greeting";
	}
 
	/**
	 * @param model
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping(value = "/finderResult", method = RequestMethod.GET)
	public String getOldestPerson(Model model) throws ParseException
	{
		model.addAttribute("oldestperson", contactManager.findOldestPerson());
		model.addAttribute("countByGender", contactManager.findcountByGender(Gender.MALE));
		model.addAttribute("ageDiffInDays", contactManager.findageDiffInDays("Bill McKnight", "Paul Robinson"));
		return "result";
	}
	
	
	
}
