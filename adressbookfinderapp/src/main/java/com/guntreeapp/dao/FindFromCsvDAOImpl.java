package com.guntreeapp.dao;

import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.guntreeapp.model.Contact;
import com.guntreeapp.model.Gender;
import com.opencsv.CSVReader;

@Repository
public class FindFromCsvDAOImpl implements FindFromCsvDAO {
	
	public ArrayList<Contact> contacts = null;

	/* (non-Javadoc)
	 * @see com.guntreeapp.dao.FindFromCsvDAO#getAllPerson()
	 */
	@Override
	public List<Contact> getAllPerson() throws ParseException {
		
		String csvFile = "src/main/resources/AddressBook.csv";
		CSVReader reader = null;
	
		try {
			reader = new CSVReader(new FileReader(csvFile));
			String[] line;
		    contacts = new ArrayList<Contact>();
			while ((line = reader.readNext()) != null) {
				Contact contect = new Contact(line[0], getGenderformat(line[1].trim()), getDateformat(line[2]));
				contacts.add(contect); 
			}
	
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return contacts;
	}
	
	/**
	 * To find the oldest person from Address book
	 * @return String name of person
	 * @throws ParseException 
	 */
	public String findOldestPerson() throws ParseException {
		getAllPerson();
		contacts.sort((Contact o1, Contact o2)->o1.getDateOfBirth().compareTo(o2.getDateOfBirth()));
		String oldPersonName=contacts.get(0).getFullName();
		return oldPersonName;
	}


	/**
	 * @param firstPersonFullName 
	 * @param secondPersonFullName
	 * @return 
	 */
	public long findageDiffInDays(String firstPersonFullName, String secondPersonFullName) {
		Contact firstPerson = findByFullName(firstPersonFullName)
				.orElseThrow(() -> new IllegalArgumentException("firstPersonFullName not found"));
		Contact secondPerson = findByFullName(secondPersonFullName)
				.orElseThrow(() -> new IllegalArgumentException("secondPersonFullName not found"));
		return Math.abs(ChronoUnit.DAYS.between(firstPerson.getDateOfBirth(), secondPerson.getDateOfBirth()));
	}


	/**
	 * @param fullName
	 * @return
	 */
	public Optional<Contact> findByFullName(String fullName) {
		Objects.requireNonNull(fullName, "fullName cannot be null");
		return contacts.stream()
				.filter(contact -> contact.getFullName().equals(fullName))
				.findFirst();
	}
	

	

	/**
	 * @param gender
	 * @return Integer Number of gender count.
	 */

	public int findcountByGender(Gender gender) {
		Objects.requireNonNull(gender, "gender cannot be null");
		return (int) contacts.stream()
				.filter(contact -> contact.getGender().equals(gender))
				.count();
	}
	

	/**
	 * @param genderString
	 * @return enum Gender.
	 * @throws ParseException
	 */
	public Gender getGenderformat(String genderString) throws ParseException {
		Gender genderValue = Gender.valueOf(genderString.toUpperCase());
		return genderValue;
	}


	/**
	 * @param dateString
	 * @return
	 * @throws ParseException
	 */
	public LocalDate getDateformat(String dateString) throws ParseException {
		Date parsedDate = new SimpleDateFormat("dd/mm/yy").parse(dateString.trim());
		return parsedDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

	}


}
