package com.guntreeapp.dao;

import java.text.ParseException;
import java.util.List;

import com.guntreeapp.model.Contact;
import com.guntreeapp.model.Gender;



/**
 * @author 
 *
 */
public interface FindFromCsvDAO {
	public List<Contact> getAllPerson() throws ParseException;
	public String findOldestPerson() throws ParseException ;
	public long findageDiffInDays(String firstPersonFullName, String secondPersonFullName);
	public int findcountByGender(Gender gender);
	
}
