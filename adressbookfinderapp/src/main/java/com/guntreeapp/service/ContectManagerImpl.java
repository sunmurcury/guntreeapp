package com.guntreeapp.service;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.guntreeapp.dao.FindFromCsvDAO;
import com.guntreeapp.model.Contact;
import com.guntreeapp.model.Gender;

@Service
public class ContectManagerImpl implements ContactManager {

	@Autowired
	FindFromCsvDAO dao;

	
	/* (non-Javadoc)
	 * @see com.guntreeapp.service.ContactManager#getAllPerson()
	 */
	public List<Contact> getAllPerson() throws ParseException
	{
		return dao.getAllPerson();
	}


	/* (non-Javadoc)
	 * @see com.guntreeapp.service.ContactManager#findOldestPerson()
	 */
	@Override
	public String findOldestPerson() throws ParseException {
		
		return dao.findOldestPerson();
	}


	/* (non-Javadoc)
	 * @see com.guntreeapp.service.ContactManager#findageDiffInDays(java.lang.String, java.lang.String)
	 */
	@Override
	public long findageDiffInDays(String firstPersonFullName, String secondPersonFullName) {
		
		return dao.findageDiffInDays(firstPersonFullName, secondPersonFullName);
	}


	/* (non-Javadoc)
	 * @see com.guntreeapp.service.ContactManager#findcountByGender(com.guntreeapp.model.Gender)
	 */
	@Override
	public int findcountByGender(Gender gender) {
		// TODO Auto-generated method stub
		return dao.findcountByGender(gender);
	}

}
