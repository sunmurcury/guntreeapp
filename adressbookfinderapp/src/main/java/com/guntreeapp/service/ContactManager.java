package com.guntreeapp.service;

import java.text.ParseException;
import java.util.List;

import org.springframework.stereotype.Component;

import com.guntreeapp.model.Contact;
import com.guntreeapp.model.Gender;

public interface ContactManager {
	
	public List<Contact> getAllPerson() throws ParseException;
	public String findOldestPerson() throws ParseException ;
	public long findageDiffInDays(String firstPersonFullName, String secondPersonFullName);
	public int findcountByGender(Gender gender);
}
