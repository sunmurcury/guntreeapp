package com.guntreeapp.model;

public enum Gender {
    FEMALE,
    MALE
}
